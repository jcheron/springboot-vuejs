package com.spring.vue;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.spring.vue.parts.VueComputeds;
import com.spring.vue.parts.VueData;
import com.spring.vue.parts.VueMethods;
import com.spring.vue.utilities.JsUtils;

/**
 * VueJS instance
 * @author jc
 * @version 1.0.0.0
 */
public class VueJS {
	private String el;
	private VueData data;
	private VueMethods methods;
	private VueComputeds computed;
	private String[] delimiters;
	
	/**
	 * @param element the DOM selector for the VueJS application
	 */
	public VueJS(String element) {
		this.el=element;
		data=new VueData();
		methods=new VueMethods();
		computed=new VueComputeds();
		this.setDelimiters("<%", "%>");
	}
	
	/**
	 * Defines the element delimiters (<% %>)
	 * @param start default: <%
	 * @param end default: %>
	 */
	public void setDelimiters(String start,String end) {
		delimiters= new String[] {start,end};
	}
	
	/**
	 * Adds a data
	 * @param key
	 * @param value
	 */
	public void addData(String key,Object value) {
		data.put(key, value);
	}
	
	/**
	 * Adds a method
	 * @param name the method name
	 * @param body the method body (javascript)
	 */
	public void addMethod(String name,String body) {
		this.addMethod(name, body, new String[]{});
	}
	
	/**
	 * Adds a method with parameters
	 * @param name the method name
	 * @param body the method body (javascript)
	 * @param params the method parameters
	 */
	public void addMethod(String name,String body,String...params) {
		methods.add(name, body, params);
	}
	
	/**
	 * Adds an editable computed property
	 * @param name
	 * @param get
	 * @param set
	 */
	public void addComputed(String name,String get,String set) {
		computed.add(name, get, set);
	}
	
	/**
	 * Adds a computed property
	 * @param name
	 * @param get
	 */
	public void addComputed(String name,String get) {
		this.addComputed(name, get, null);
	}
	
	/**
	 * @return the generated script (javascript)
	 */
	public String getScript() {
		try {
			String script="new Vue("+JsUtils.objectToJSON(this)+")";
			return "<script>"+script+"</script>";
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return "";
	}
	
	public String getEl() {
		return el;
	}

	public VueData getData() {
		return data;
	}

	public VueMethods getMethods() {
		return methods;
	}

	public String[] getDelimiters() {
		return delimiters;
	}

	public VueComputeds getComputed() {
		return computed;
	}
}
